import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

data = np.loadtxt('data/HOM_sorted.txt')

def gaus(x,a,sigma,b):
    return -a*np.exp(-(x-np.min(data[:,0]))**2/(2*sigma**2))+b

p0 = [ 1600, 2, 600]
coeff,covar_matrix = curve_fit(gaus,data[:,0],data[:,1],p0=p0)
x = np.linspace(-np.max(data[:,0]) + 2 * np.min(data[:,0]) - 1, np.max(np.max(data[:,0])) + 1)

plt.xlim(-8.2,8.2)
plt.xlabel('Position ($\mu m$)')
plt.ylabel('No. of cooincidence counts in 1 second')
plt.errorbar(data[:,0]-6, data[:,1], xerr=0.5, yerr=200)
plt.plot( x-6, gaus(x, *coeff), 'r-')
#plt.plot(data[:,0], data[:,1], 'ro', data[:,0], gaus(data[:,0], popt[0], popt[1], popt[2], popt[3]), 'r-')
sigma = coeff[1]
print ('FWHM is: ', 2.35482*sigma, 'microns')

#dw = 2*pi*f
#Tcorr = 1/dw
plt.savefig('HOM_figure.pdf')


plt.show()
