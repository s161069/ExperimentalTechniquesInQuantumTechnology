from time import sleep
import numpy as np
import serial
import visa

### Settings
theta_1 = 0    # Degrees
theta_2 = 45
phi_1   = 22.5
phi_2   = 67.5

### Constants
steps_per_degree = 9600 / 360	# Given my quTOOLS or by using the "refall" followed by "wat" in the serial terminal
encoding = 'iso-8859-1'
time_range = 0.06           # microseconds
voltage_offset = -10        # volts
voltage_range = 20          # volts
counts_per_segment = 1000   # maximum is 1000
segments = 1
orthogonality = 90

### Open connection with oscilloscope
rm = visa.ResourceManager()
oscilloscope = rm.open_resource('USB0::0x2A8D::0x1766::MY56311270::INSTR')
oscilloscope.write(':ACQuire:TYPE NORMal')
oscilloscope.write(':ACQuire:MODE SEGMented')
oscilloscope.write(':ACQuire:SEGMented:COUNt ' + str(counts_per_segment))
oscilloscope.write(':TIMebase:MODE MAIN')
oscilloscope.write(':TIMebase:RANGe ' + str(time_range / 1000000))
oscilloscope.write(':WAVeform:BYTeorder LSBFirst') # little-endian
oscilloscope.write(':WAVeform:FORMat BYTE')        # 8 bits
oscilloscope.write(':WAVeform:TYPE NORM')
oscilloscope.write(':WAVeform:UNSigned ON')
oscilloscope.write(':WAVeform:POINTS:MODE MAXimum')
oscilloscope.write(':WAVeform:POINTS 8000000')
oscilloscope.write(':CHANnel1:IMPedance FIFTy')
oscilloscope.write(':CHANnel3:IMPedance FIFTy')
oscilloscope.write(':CHANnel1:RANGe ' + str(voltage_range) + 'V')
oscilloscope.write(':CHANnel3:RANGe ' + str(voltage_range) + 'V')
#oscilloscope.write(':CHAnnel1:OFFSet ' + str(voltage_offset) + 'V')
#oscilloscope.write(':CHAnnel3:OFFSet ' + str(voltage_offset) + 'V')
oscilloscope.write(':TRIGger:MODE EDGE')
oscilloscope.write(':TRIGger:EDGE:COUPling DC')
oscilloscope.write(':TRIGger:EDGE:LEVel 3.625')
oscilloscope.write(':TRIGger:EDGE:COUPling REJect OFF')
oscilloscope.write(':TRIGger:EDGE:COUPling SLOPe POS')
oscilloscope.write(':TRIGger:EDGE:SOURce CHANnel1')
oscilloscope.write(':TRIGger:ZONE:SOURce CHANnel3')
oscilloscope.write(':TROGger:ZONE:STATe ON')
oscilloscope.write(':TRIGger:ZONE1:MODE INTersect')
oscilloscope.write(':TRIGger:ZONE1:PLACement +2.924000E-008,+2.000000E+000,-2.720000E-009,+1.875000E+000')
oscilloscope.write(':TRIGger:ZONE1:STATe ON')
oscilloscope.write(':TRIGger:ZONE2:STATe OFF')

### Open connection with motors
motors = serial.Serial(
	port='/dev/tty.usbserial-AIW50CK',
	baudrate=115200,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS
)
motors.close()
motors.open()
assert motors.is_open
motors.reset_input_buffer()
motors.reset_output_buffer()

### Do the test
def coincidences(theta, phi):
	# Set polarizers
	motors.write(b'go ' + bytes(str(theta * steps_per_degree), encoding=encoding) + b' ' + bytes(str(phi * steps_per_degree), encoding=encoding) + b" 0\n")
	motors.flush()
	sleep(1)

	# Run
	time = 0.0
	for j in range(segments):
		oscilloscope.write(':SINGle')

		sleep(1)
		while int(oscilloscope.query(':WAVeform:SEGMented:COUNt?')) < counts_per_segment:
			sleep(1)

		oscilloscope.write(':ACQuire:SEGMented:INDex ' + str(counts_per_segment))
		ttag = float(oscilloscope.query(':WAVeform:SEGMented:TTAG?'))
		time = time + ttag

	# Results
	coincidences_count = segments * counts_per_segment / time
	print ('measurement:', theta, phi, coincidences_count)
	#input()
	return coincidences_count

def C(theta, phi):
	AA = coincidences(theta, phi)
	AB = coincidences(theta, phi + orthogonality)
	BA = coincidences(theta + orthogonality, phi)
	BB = coincidences(theta + orthogonality, phi + orthogonality)
	C = (AA - AB - BA + BB) / (AA + BB + BA + AB)
	print ('C:', theta, phi, C)
	return C

S = C(theta_1, phi_1) - C(theta_1, phi_2) + C(theta_2, phi_1) + C(theta_2, phi_2)
print ('S:', S)

### The End
motors.write(b'go 0 0 0\n')
motors.flush()
sleep(1)
motors.close()
oscilloscope.close()
rm.close()
