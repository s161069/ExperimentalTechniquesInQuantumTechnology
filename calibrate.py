from time import sleep
import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt
import serial
import visa

### Constants
steps = 9600
step_step = 100
encoding = 'iso-8859-1'
time_range = 0.06           # microseconds
voltage_offset = -10        # volts
voltage_range = 20          # volts
counts_per_segment = 100    # maximum is 1000
segments = 1

### Open connection with oscilloscope
rm = visa.ResourceManager()
oscilloscope = rm.open_resource('USB0::0x2A8D::0x1766::MY56311270::INSTR')
oscilloscope.write(':ACQuire:TYPE NORMal')
oscilloscope.write(':ACQuire:MODE SEGMented')
oscilloscope.write(':ACQuire:SEGMented:COUNt ' + str(counts_per_segment))
oscilloscope.write(':TIMebase:MODE MAIN')
oscilloscope.write(':TIMebase:RANGe ' + str(time_range / 1000000))
oscilloscope.write(':WAVeform:BYTeorder LSBFirst') # little-endian
oscilloscope.write(':WAVeform:FORMat BYTE')        # 8 bits
oscilloscope.write(':WAVeform:TYPE NORM')
oscilloscope.write(':WAVeform:UNSigned ON')
oscilloscope.write(':WAVeform:POINTS:MODE MAXimum')
oscilloscope.write(':WAVeform:POINTS 8000000')
oscilloscope.write(':CHANnel1:IMPedance FIFTy')
oscilloscope.write(':CHANnel3:IMPedance FIFTy')
oscilloscope.write(':CHANnel1:RANGe ' + str(voltage_range) + 'V')
oscilloscope.write(':CHANnel3:RANGe ' + str(voltage_range) + 'V')
#oscilloscope.write(':CHAnnel1:OFFSet ' + str(voltage_offset) + 'V')
#oscilloscope.write(':CHAnnel3:OFFSet ' + str(voltage_offset) + 'V')
oscilloscope.write(':TRIGger:MODE EDGE')
oscilloscope.write(':TRIGger:EDGE:COUPling DC')
oscilloscope.write(':TRIGger:EDGE:LEVel 3.625')
oscilloscope.write(':TRIGger:EDGE:COUPling REJect OFF')
oscilloscope.write(':TRIGger:EDGE:COUPling SLOPe POS')
oscilloscope.write(':TRIGger:EDGE:SOURce CHANnel1')
oscilloscope.write(':TRIGger:ZONE:SOURce CHANnel3')
oscilloscope.write(':TROGger:ZONE:STATe ON')
oscilloscope.write(':TRIGger:ZONE1:MODE INTersect')
oscilloscope.write(':TRIGger:ZONE1:PLACement +2.924000E-008,+2.000000E+000,-2.720000E-009,+1.875000E+000')
oscilloscope.write(':TRIGger:ZONE1:STATe ON')
oscilloscope.write(':TRIGger:ZONE2:STATe OFF')

### Open connection with motors
motors = serial.Serial(
	port='/dev/tty.usbserial-AIW50CK',
	baudrate=115200,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS
)
motors.close()
motors.open()
assert motors.is_open
motors.reset_input_buffer()
motors.reset_output_buffer()

### Calibration
def coincidences(s1, s2):
	# Set polarizers
	motors.write(b'go ' + bytes(str(s1), encoding=encoding) + b' ' + bytes(str(s2), encoding=encoding) + b" 0\n")
	motors.flush()
	sleep(1)

	# Run
	time = 0.0
	for j in range(segments):
		oscilloscope.write(':SINGle')

		sleep(1)
		while int(oscilloscope.query(':WAVeform:SEGMented:COUNt?')) < counts_per_segment:
			sleep(1)

		oscilloscope.write(':ACQuire:SEGMented:INDex ' + str(counts_per_segment))
		ttag = float(oscilloscope.query(':WAVeform:SEGMented:TTAG?'))
		time = time + ttag

	# Results
	coincidences_count = segments * counts_per_segment / time
	return coincidences_count

### Calibrate
def calibrate(motor, other, angle_scale):
	assert motor == 0 or motor == 1
	func = lambda x, offset, amplitude: amplitude * np.square(np.sin(angle_scale * 2 * np.pi * (x - offset) / steps))
	xdata = np.arange(0, steps, step_step)
	ydata = np.empty_like(xdata)
	for i in range(len(xdata)):
		if (motor == 0):
			ydata[i] = coincidences(xdata[i], other)
		else:
			ydata[i] = coincidences(other, xdata[i])
		print (int(i / len(xdata) * 100), '%', xdata[i], ydata[i])
	np.save('calibration.npy', ydata)
	popt, pcov = optimize.curve_fit(func, xdata, ydata)
	plt.plot(xdata, ydata, xdata, func(xdata, popt[0], popt[1]))
	plt.show()
	return popt[0]

max1 = calibrate(0, 0, 2)
max2 = 0 # Need to calibrate polarizing filter manually
print (max1, max2)

### Save
motors.write(b'go ' + bytes(str(int(max1)), encoding=encoding) + b' ' + bytes(str(int(max2)), encoding=encoding) + b' 0\n')
motors.write(b'setMotor 0\n')
motors.write(b'resetPos\n')
motors.write(b'setMotor 1\n')
motors.write(b'resetPos\n')
motors.write(b'go 0 0 0\n')
motors.flush()
sleep(1)

### The End
motors.write(b'go 0 0 0\n')
motors.flush()
sleep(1)
motors.close()
oscilloscope.close()
rm.close()
