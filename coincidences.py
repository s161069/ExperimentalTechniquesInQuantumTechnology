import numpy as np
import matplotlib.pyplot as plt
import visa
from time import sleep

### Settings
time_range = 0.06           # microseconds
voltage_offset = -10        # volts
voltage_range = 20          # volts
counts_per_segment = 1000     # maximum is 1000
segments = 1

### Open connection
rm = visa.ResourceManager()
inst = rm.open_resource('USB0::0x2A8D::0x1766::MY56311270::INSTR')

### Initialize
inst.write(':ACQuire:TYPE NORMal')
inst.write(':ACQuire:MODE SEGMented')
inst.write(':ACQuire:SEGMented:COUNt ' + str(counts_per_segment))
inst.write(':ACQuire:SEGMented:INDex ' + str(counts_per_segment))
inst.write(':TIMebase:MODE MAIN')
inst.write(':TIMebase:RANGe ' + str(time_range / 1000000))
inst.write(':WAVeform:BYTeorder LSBFirst') # little-endian
inst.write(':WAVeform:FORMat BYTE')        # 8 bits
inst.write(':WAVeform:TYPE NORM')
inst.write(':WAVeform:UNSigned ON')
inst.write(':WAVeform:POINTS:MODE MAXimum')
inst.write(':WAVeform:POINTS 8000000')
inst.write(':CHANnel1:IMPedance FIFTy')
inst.write(':CHANnel3:IMPedance FIFTy')
inst.write(':CHANnel1:RANGe ' + str(voltage_range) + 'V')
inst.write(':CHANnel3:RANGe ' + str(voltage_range) + 'V')
#inst.write(':CHAnnel1:OFFSet ' + str(voltage_offset) + 'V')
#inst.write(':CHAnnel3:OFFSet ' + str(voltage_offset) + 'V')
inst.write(':TRIGger:MODE EDGE')
inst.write(':TRIGger:EDGE:COUPling DC')
inst.write(':TRIGger:EDGE:LEVel 3.625')
inst.write(':TRIGger:EDGE:COUPling REJect OFF')
inst.write(':TRIGger:EDGE:COUPling SLOPe POS')
inst.write(':TRIGger:EDGE:SOURce CHANnel1')
inst.write(':TRIGger:ZONE:SOURce CHANnel3')
inst.write(':TROGger:ZONE:STATe ON')
inst.write(':TRIGger:ZONE1:MODE INTersect')
inst.write(':TRIGger:ZONE1:PLACement +2.924000E-008,+2.000000E+000,-2.720000E-009,+1.875000E+000')
inst.write(':TRIGger:ZONE1:STATe ON')
inst.write(':TRIGger:ZONE2:STATe OFF')

### Run
time = 0.0
for i in range(segments):
    inst.write(':SINGle')
    sleep(0.5) # Give oscilloscope time to run

	# Get segment length
    ttag = float(inst.query(':WAVeform:SEGMented:TTAG?'))
    time = time + ttag
    print (ttag, time)

### Results
coincidences = counts_per_segment * segments
print ('Coincidences:', coincidences)
print ('Measurement time:', time, 's')
print ('Coincidences per second:', coincidences / time)
