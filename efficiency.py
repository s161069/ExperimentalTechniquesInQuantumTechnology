import numpy as np
import matplotlib.pyplot as plt
import visa
from time import sleep

### Settings
time_range = 0.06           # microseconds
voltage_offset = -10        # volts
voltage_range = 20          # volts
counts_per_segment = 10     # maximum is 1000
segments = 3

### Open connection
rm = visa.ResourceManager()
inst = rm.open_resource('USB0::0x2A8D::0x1766::MY56311270::INSTR')

### Initialize
inst.write(':ACQuire:TYPE NORMal')
inst.write(':ACQuire:MODE SEGMented')
inst.write(':ACQuire:SEGMented:COUNt ' + str(counts_per_segment))
inst.write(':TIMebase:MODE MAIN')
inst.write(':TIMebase:RANGe ' + str(time_range / 1000000))
inst.write(':WAVeform:BYTeorder LSBFirst') # little-endian
inst.write(':WAVeform:FORMat BYTE')        # 8 bits
inst.write(':WAVeform:TYPE NORM')
inst.write(':WAVeform:UNSigned ON')
inst.write(':WAVeform:POINTS:MODE MAXimum')
inst.write(':WAVeform:POINTS 8000000')
inst.write(':CHANnel1:IMPedance FIFTy')
inst.write(':CHANnel3:IMPedance FIFTy')
inst.write(':CHANnel1:RANGe ' + str(voltage_range) + 'V')
inst.write(':CHANnel3:RANGe ' + str(voltage_range) + 'V')
#inst.write(':CHAnnel1:OFFSet ' + str(voltage_offset) + 'V')
#inst.write(':CHAnnel3:OFFSet ' + str(voltage_offset) + 'V')
inst.write(':TRIGger:MODE EDGE')
inst.write(':TRIGger:EDGE:COUPling DC')
inst.write(':TRIGger:EDGE:LEVel 3.625')
inst.write(':TRIGger:EDGE:COUPling REJect OFF')
inst.write(':TRIGger:EDGE:COUPling SLOPe POS')
inst.write(':TRIGger:EDGE:SOURce CHANnel1')
inst.write(':TRIGger:ZONE1:STATe OFF')
inst.write(':TRIGger:ZONE2:STATe OFF')

### Run
time = 0.0
coincidences = 0
channel1 = np.array([])
channel2 = np.array([])
for i in range(segments):
    inst.write(':SINGle')
    sleep(0.3) # Give oscilloscope time to run

    for i in range(1, counts_per_segment + 1):
        inst.write(':ACQuire:SEGMented:INDex ' + str(i))
        inst.write(':WAVeform:SOURce CHANnel1')
        data1 = inst.query_binary_values(':WAVeform:DATA?', container=np.array, is_big_endian=False, datatype = 'B')
        inst.write(':WAVeform:SOURce CHANnel3')
        data2 = inst.query_binary_values(':WAVeform:DATA?', container=np.array, is_big_endian=False, datatype = 'B')

        if (np.max(data2) - np.min(data2) > (np.max(data1) - np.min(data1)) / 2):
            coincidences = coincidences + 1

        channel1 = np.concatenate((channel1, data1))
        channel2 = np.concatenate((channel2, data2))

	# Get segment length
    ttag = float(inst.query(':WAVeform:SEGMented:TTAG?'))
    time = time + ttag
    print (ttag, time, coincidences)

channel1 = channel1.reshape(segments, counts_per_segment, -1)
channel2 = channel2.reshape(segments, counts_per_segment, -1)

### Results
print ('Measurement time:', time, 's')
print ('Counts:', segments * counts_per_segment)
print ('Counts per second:', segments * counts_per_segment / time)
print ('Coincidences per second:', coincidences / time)
print ('Coincidences:', coincidences)
print ('Efficinecy:', coincidences / (segments * counts_per_segment))
np.savez_compressed('data.npz', channel1=channel1, channel2=channel2)
