#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void calculate(double positions1[], double positions2[], int positions1_length, int positions2_length, double scale, double *results, int results_length)
{
	for (int i = 0; i < positions1_length; i++)
	{
		for (int j = 0; j < positions2_length; j++)
		{
			double diff = positions2[j] - positions1[i];
			int index = round(diff / scale) + results_length / 2;
			if (index >= 0 && index < results_length)
				results[index]++;
		}
//		printf("%f%%\n", 100. * i / positions1_length);
	}
}
