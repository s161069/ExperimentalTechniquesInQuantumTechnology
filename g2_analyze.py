import os
from time import sleep
import ctypes
import numpy.ctypeslib as npct
import numpy as np
import matplotlib.pyplot as plt

### Settings
scale = 25 / 1000.          # microseconds
g2_border = 20              # microseconds
clicks_file = 'clicks.npz'

### Load clicks
assert os.path.isfile(clicks_file)
data = np.load(clicks_file)
positions1 = data['channel1']
positions2 = data['channel2']
print ('Clicks file loaded')
number_of_runs = len(positions1)

### Analyze
# Constants
g2_range = 2 * g2_border
g2_len = int(np.round(g2_range / scale)) + 1
g2 = np.zeros(g2_len)
t = np.linspace(-g2_border, g2_border, g2_len)
# Load C library
double_array = npct.ndpointer(dtype=np.double, ndim=1, flags='CONTIGUOUS')
clib = npct.load_library('libg2.so', '.')
clib.calculate.restype = None
clib.calculate.argtypes = [ double_array, double_array, ctypes.c_int, ctypes.c_int, ctypes.c_double, double_array, ctypes.c_int]
# Run
for run in range(number_of_runs):
	clib.calculate(positions1[run], positions2[run], len(positions1[run]), len(positions2[run]), scale, g2, len(g2))
# Save results
np.savez_compressed('g2_result.npz', g2=g2, t=t)
# Plot
plt.ylabel('Number of coincidences per ' + str(int(np.round(scale * 1000))) + ' nanoseconds')
plt.xlabel('t ($\mu s$)')
plt.plot(t, g2)
plt.savefig('g2.pdf')
plt.show()
