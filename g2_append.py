import numpy as np
from glob import glob

channel1 = []
channel2 = []
number_of_bins = []

for f in glob('clicks/*.npz'):
	data = np.load(f)
	cur1 = data['channel1']
	cur2 = data['channel2']
	curN = data['number_of_bins']
	for i in range(len(cur1)):
		channel1.append(cur1[i])
		channel2.append(cur2[i])
		number_of_bins.append(curN[i])

np.savez_compressed('clicks.npz', channel1=channel1, channel2=channel2, number_of_bins=number_of_bins)
