import os
from glob import glob
from time import sleep
import ctypes
import numpy.ctypeslib as npct
import numpy as np
import matplotlib.pyplot as plt

### Settings
our_data = True
delay_trigger = 0.021       # microseconds

### Helper functions
def shift(a, i):
	l = len(a)
	if (i > 0):
		return np.pad(a, (i,  0), mode='constant')[0:l]
	else:
	 	return np.pad(a, (0, -i), mode='constant')[-i:l-i]

def find_clicks(channel):
	time_step = time_range / len(channel1[run]) # microseconds per pixel
	positions = []
	group_length        = [ 0, 0 ]
	group_length_needed = [ 10, 2 ]
	for i in range(len(channel)):
		if channel[i] == channel[i - 1] and i >= 1:
			group_length[channel[i]] += 1
		else:
			group_length[channel[i]] = 1

		if channel[i] == 1 and group_length[0] > group_length_needed[0] and group_length[1] == group_length_needed[1]:
			# Found a click
			positions.append(i * time_step)
	return np.array(positions)

### Main part
if our_data:
	for data_file in glob('segments/*.npz'):
		clicks_file = data_file.replace('segments', 'clicks')
		assert data_file != clicks_file
		if not os.path.isfile(clicks_file):
			### Get data
			assert os.path.isfile(data_file)
			data = np.load(data_file)
			channel1 = data['channel1']
			channel2 = data['channel2']
			herald   = data['channel3']
			time_range = data['time_range'][0]
			assert len(channel1) == len(channel2) and len(channel2) == len(herald)

			### Find clicks
			positions1 = []
			positions2 = []
			number_of_bins = []
			for run in range(len(channel1)):
				assert len(channel1[run]) == len(channel2[run]) and len(channel2[run]) == len(herald[run])
				number_of_bins.append(len(channel1[run]))

				# Digitize
				delay_index = int(np.round(delay_trigger / time_range * len(herald[run])))
				rolled_herald = shift(herald[run], delay_index)
				treshold = np.max(rolled_herald) / 2
				binary1 = np.where(channel1[run] > treshold, np.ones_like(channel1[run]), np.zeros_like(channel1[run]))
				binary2 = np.where(channel2[run] > treshold, np.ones_like(channel2[run]), np.zeros_like(channel2[run]))
				binaryh = np.where(rolled_herald > treshold, np.ones_like(rolled_herald), np.zeros_like(rolled_herald))

				# Herald
				heralded1 = binary1 * binaryh
				heralded2 = binary2 * binaryh

				# Find the beginnings of clicks
				positions1.append(find_clicks(heralded1))
				positions2.append(find_clicks(heralded2))

			np.savez(clicks_file, channel1=positions1, channel2=positions2, number_of_bins=number_of_bins)
			print ('Clicks file saved')
else:
	def herald(trace, trigger):
		epsilon = 5 / 1000. # microseconds
		ret = []
		t = 0
		for i in trace:
			while trigger[t] < i - epsilon and t < len(trigger) - 1:
				t += 1
			if np.abs(trigger[t] - i) < epsilon:
				ret.append(i)
		return np.array(ret)

	trace1 = np.loadtxt('trace1.txt') * 1e6
	trace2 = np.loadtxt('trace2.txt') * 1e6
	trigger = (np.loadtxt('traceTrigger.txt') + 13e-9) * 1e6

	positions1 = [herald(trace1, trigger)]
	positions2 = [herald(trace2, trigger)]
	time_range = np.max(trigger)

	np.savez('clicks.npz', channel1=positions1, channel2=positions2)
	print ('Clicks file saved')
