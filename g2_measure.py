import os
from time import sleep, time
import ctypes
import numpy.ctypeslib as npct
import numpy as np
import matplotlib.pyplot as plt
import visa

### Settings
runs = 100
runs_per_file = 20

### Constants
time_range = 1000          # microseconds
voltage_offset = -10        # volts
voltage_range = 20          # volts
data_file = 'g2.npz'

### Get data
# Open connection with oscilloscope
def open_oscilloscope():
	rm = visa.ResourceManager()
	oscilloscope = rm.open_resource('USB0::0x2A8D::0x1766::MY56311277::INSTR')
	oscilloscope.write(':ACQuire:TYPE NORMal')
	oscilloscope.write(':ACQuire:MODE RTIMe')
	oscilloscope.write(':TIMebase:MODE MAIN')
	oscilloscope.write(':TIMebase:SCALe ' + str(time_range / 1000000 / 10))
	oscilloscope.write(':TIMebase:DELay 0') # Center oscilloscope window around 0
	oscilloscope.write(':WAVeform:BYTeorder LSBFirst') # little-endian
	oscilloscope.write(':WAVeform:FORMat BYTE')        # 8 bits
	oscilloscope.write(':WAVeform:TYPE NORM')
	oscilloscope.write(':WAVeform:UNSigned ON')
	oscilloscope.write(':WAVeform:POINTS:MODE RAW')
	oscilloscope.write(':WAVeform:POINTS 8000000')
	oscilloscope.write(':CHANnel1:IMPedance FIFTy')
	oscilloscope.write(':CHANnel2:IMPedance FIFTy')
	oscilloscope.write(':CHANnel3:IMPedance FIFTy')
	oscilloscope.write(':CHANnel1:RANGe ' + str(voltage_range) + 'V')
	oscilloscope.write(':CHANnel2:RANGe ' + str(voltage_range) + 'V')
	oscilloscope.write(':CHANnel3:RANGe ' + str(voltage_range) + 'V')
	#oscilloscope.write(':CHAnnel1:OFFSet ' + str(voltage_offset) + 'V')
	#oscilloscope.write(':CHAnnel2:OFFSet ' + str(voltage_offset) + 'V')
	#oscilloscope.write(':CHAnnel3:OFFSet ' + str(voltage_offset) + 'V')
	return oscilloscope

# Capture data
channel1 = []
channel2 = []
herald = []
i = 1
while i <= runs:
	try:
		oscilloscope.write(':SINGle')
		sleep(1)
		oscilloscope.write(':STOP')
		sleep(0.1)

		oscilloscope.write(':WAVeform:SOURce CHANnel1')
		current1 = oscilloscope.query_binary_values(':WAVeform:DATA?', container=np.array, is_big_endian=False, datatype = 'B')
		sleep(0.1)
		oscilloscope.write(':WAVeform:SOURce CHANnel2')
		current2 = oscilloscope.query_binary_values(':WAVeform:DATA?', container=np.array, is_big_endian=False, datatype = 'B')
		sleep(0.1)
		oscilloscope.write(':WAVeform:SOURce CHANnel3')
		current3 = oscilloscope.query_binary_values(':WAVeform:DATA?', container=np.array, is_big_endian=False, datatype = 'B')
		sleep(0.1)


		current1 = current1 - np.min(current1)
		current2 = current2 - np.min(current2)
		current3 = current3 - np.min(current3)

		channel1.append(current1)
		channel2.append(current2)
		herald.append(current3)

		print ('Number of points per run:', len(channel1[-1]), len(channel2[-1]), len(herald[-1]))

		if len(channel1) == runs_per_file:
			np.savez_compressed(str(time()) + '.npz', channel1=channel1, channel2=channel2, channel3=herald, time_range=[time_range])
			channel1 = []
			channel2 = []
			herald = []
		i = i + 1
	except Exception as error:
		print ('ERROR!!!', error)
		print ('\a')
		sleep(10)
		try:
			oscilloscope.close()
			sleep(5)
			oscilloscope = open_oscilloscope()
		except:
			pass

oscilloscope.close()
