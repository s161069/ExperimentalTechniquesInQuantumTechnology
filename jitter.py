import numpy as np
import matplotlib.pyplot as plt

time_range = 60 # nanoseconds
mint = -10
maxt = 10

# Load data
npz = np.load('data/efficiency.npz')
channel1 = npz['channel1']
channel2 = npz['channel2']
time_step = time_range / len(channel1[0, 0])
print ('Number of coincidences:', len(channel1) * len(channel1[0]))

# Find coincidences
max1 = channel1.max(axis = 2) - channel1.min(axis = 2)
max2 = channel2.max(axis = 2) - channel2.min(axis = 2)
valid = np.where(max2 > max1 / 2., np.ones_like(max1), np.zeros_like(max1)).flatten()

# Calculate delay
def beginnings(channel):
	# Maximum slope is taken as a beginning of pulse
	gradient = np.gradient(channel, axis=2)
	argmax = np.argmax(gradient, axis=2)
	_, _, indeces = np.unravel_index(argmax, gradient.shape)
	return indeces.flatten()

b1 = beginnings(channel1)
b2 = beginnings(channel2)
delay = b2 - b1
valid_delay = delay[valid == 1] # Only use coincidences
window = valid_delay[(mint / time_step <= valid_delay) & (valid_delay <= maxt / time_step)]

# Standard deviance
print ('Standard deviance:', np.std(window) * time_step, 'ns')
print ('Mean:', np.mean(window) * time_step, 'ns')

# Plot all
plt.xlabel('time difference (ns)')
plt.ylabel('number of coincidences')
n, bins, patches = plt.hist(valid_delay * time_step, bins = (np.max(valid_delay) - np.min(valid_delay) + 1))
plt.show()

# Plot window
plt.xlabel('time difference (ns)')
plt.ylabel('number of coincidences')
n, bins, patches = plt.hist(window * time_step, bins = (np.max(window) - np.min(window) + 1))
plt.savefig('jitter_figure.pdf')
plt.show()
