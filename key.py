import time
import random
import numpy as np
import serial
import visa

### Settings
initial_bits_needed = 100
number_of_passes = 4 # Error correction

### Constants
steps_per_degree = 9600 / 360  # Given by quTOOLS or by using the "refall" followed by "wat" in the serial terminal
encoding = 'iso-8859-1'
time_range = 0.06              # microseconds
voltage_offset = -10           # volts
voltage_range = 20             # volts
motor_wait_time = 0.6          # seconds
oscilloscope_wait_time = 0.01  # seconds

### Open connection with oscilloscope
rm = visa.ResourceManager()
oscilloscope = rm.open_resource('USB0::0x2A8D::0x1766::MY56311270::INSTR')
oscilloscope.write(':ACQuire:TYPE NORMal')
oscilloscope.write(':ACQuire:MODE RTIMe')
oscilloscope.write(':TIMebase:MODE MAIN')
oscilloscope.write(':TIMebase:RANGe ' + str(time_range / 1000000))
oscilloscope.write(':WAVeform:BYTeorder LSBFirst') # little-endian
oscilloscope.write(':WAVeform:FORMat BYTE')        # 8 bits
oscilloscope.write(':WAVeform:TYPE NORM')
oscilloscope.write(':WAVeform:UNSigned ON')
oscilloscope.write(':WAVeform:POINTS:MODE MAXimum')
oscilloscope.write(':WAVeform:POINTS 8000000')
oscilloscope.write(':CHANnel1:IMPedance FIFTy')
oscilloscope.write(':CHANnel3:IMPedance FIFTy')
oscilloscope.write(':CHANnel1:RANGe ' + str(voltage_range) + 'V')
oscilloscope.write(':CHANnel3:RANGe ' + str(voltage_range) + 'V')
#oscilloscope.write(':CHAnnel1:OFFSet ' + str(voltage_offset) + 'V')
#oscilloscope.write(':CHAnnel3:OFFSet ' + str(voltage_offset) + 'V')
oscilloscope.write(':TRIGger:MODE EDGE')
oscilloscope.write(':TRIGger:EDGE:COUPling DC')
oscilloscope.write(':TRIGger:EDGE:LEVel 2')
oscilloscope.write(':TRIGger:EDGE:COUPling REJect OFF')
oscilloscope.write(':TRIGger:EDGE:COUPling SLOPe POS')
oscilloscope.write(':TRIGger:EDGE:SOURce CHANnel3')
oscilloscope.write(':TRIGger:ZONE1:STATe OFF')
oscilloscope.write(':TRIGger:ZONE2:STATe OFF')

### Open connection with motors
motors = serial.Serial(
	port='/dev/tty.usbserial-AIW50CK',
	baudrate=115200,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS
)
motors.close()
motors.open()
assert motors.is_open
motors.reset_input_buffer()
motors.reset_output_buffer()

### Get initial bits
# B92 protocol, see http://www.cki.au.dk/experiment/qrypto/doc/QuCrypt/b92coding.html for information
def set_basises():
	steps = lambda position, scale: bytes(str(scale * (position * 45) * steps_per_degree), encoding=encoding)
	alice = random.randint(0, 1)
	bob   = random.randint(0, 1)
	motors.write(b'go ' + steps(alice, +0.5) + b' ' + steps(1 - bob, -1) + b'\n')
	motors.flush()
	return alice, bob
initial_bits_alice = []
initial_bits_bob = []
alice, bob = set_basises()
while len(initial_bits_bob) < initial_bits_needed:
	# Run
	oscilloscope.write(':SINGle')
	time.sleep(oscilloscope_wait_time)
	next_alice, next_bob = set_basises()

	# Read values
	time_before = time.time()
	oscilloscope.write(':WAVeform:SOURce CHANnel1')
	channel1 = oscilloscope.query_binary_values(':WAVeform:DATA?', container=np.array, is_big_endian=False, datatype = 'B')
	oscilloscope.write(':WAVeform:SOURce CHANnel3')
	channel2 = oscilloscope.query_binary_values(':WAVeform:DATA?', container=np.array, is_big_endian=False, datatype = 'B')

	# Analyze
	if np.max(channel1) - np.min(channel1) > (np.max(channel2) - np.min(channel2)) / 2:
		# At this point one would connect over classical channel to confirm receival
		initial_bits_alice.append(alice)
		initial_bits_bob.append(bob)
		print ('Alice has ' + str(alice) + ' and Bob has ' + str(bob) + '. In total, ' + str(len(initial_bits_bob)) + ' bits have been sent.')
		print ('Errors so far:', np.sum(np.abs(np.array(initial_bits_bob) - np.array(initial_bits_alice))))
	else:
		print ('No luck')

	# Wait for motors
	time_after = time.time()
	time_spent = time_after - time_before
	if time_spent < motor_wait_time:
		time.sleep(motor_wait_time - time_spent)
	else:
		print ('Time spent:', time_spent)

	# Next bits
	alice = next_alice
	bob = next_bob

initial_bits_alice = np.array(initial_bits_alice)
initial_bits_bob   = np.array(initial_bits_bob)

### Error rate
error_bits_alice = initial_bits_alice[1::2]
error_bits_bob = initial_bits_bob[1::2]
error_bits = np.abs(error_bits_alice - error_bits_bob) # Connection over classical channel
error_rate = np.sum(error_bits) / len(error_bits)
print ('Error rate:', error_rate)

### Initial key
initial_key_alice = initial_bits_alice[0::2]
initial_key_bob = initial_bits_bob[0::2]
initial_key_length = int(np.ceil(initial_bits_needed / 2))
print ('Initial key: Alice has\n', initial_key_alice, '\nand Bob has\n', initial_key_bob)

### Error correction
# Cascade protocol, see www.dtic.mil/dtic/tr/fulltext/u2/a557404.pdf for description of algorithm
public_bits = 0

# Divide key into blocks
blocks = [] # Array of indeces for initial_key arrays, 3D (pass, block, index)
for i in range(number_of_passes):
	number_of_blocks = int(np.ceil(np.sqrt(initial_key_length) / np.power(2, i)))
	permutation = np.random.permutation(initial_key_length) # Connection over classical channel
	block_edges = np.round(np.arange(1, number_of_blocks) * initial_key_length / number_of_blocks).astype(int)
	blocks.append(np.split(permutation, block_edges))

# Binary search to find the faulty bit
def correct_error(pass_number, block_number, start, end):
	global public_bits

	# Need to check if faulty bit is in given range
	parity_alice = np.mod(np.sum(initial_key_alice[blocks[pass_number][block_number][start:end]]), 2)
	parity_bob   = np.mod(np.sum(initial_key_bob  [blocks[pass_number][block_number][start:end]]), 2)
	error = np.abs(parity_alice - parity_bob) # Connection over classical channel
	public_bits = public_bits + 1

	if error:
		print ('Found faulty bit in pass', pass_number, ' block', block_number, 'between', start, 'and', end)
		# Locate faulty bit
		if start == end - 1:
			# Found the faulty bit
			initial_key_bob[blocks[pass_number][block_number][start]] = 1 - initial_key_bob[blocks[pass_number][block_number][start]]
			print ('Fixed faulty bit')
		else:
			# Continue binary search recursively
			mid = int(np.floor((end - start + 1) / 2 + start))
			first = correct_error(pass_number, block_number, start, mid)
			if not first:
				correct_error(pass_number, block_number, mid, end)
	return error

# Run error correction
found_errors = True
while found_errors:
	found_errors = False
	for pass_number in range(number_of_passes):
		for block_number in range(len(blocks[pass_number])):
			found_errors = correct_error(pass_number, block_number, 0, len(blocks[pass_number][block_number])) or found_errors
		if found_errors and pass_number > 1:
			break

# Result
print ('Error corrected key: Alice has\n', initial_key_alice, '\nand Bob has\n', initial_key_bob)
print ('Made', public_bits, 'bits public')

### Privacy amplification
final_length = initial_key_length - public_bits
block_edges = np.round(np.arange(1, final_length) * initial_key_length / final_length).astype(int)
blocks_alice = np.split(initial_key_alice, block_edges)
blocks_bob   = np.split(initial_key_bob,   block_edges)
key_alice = np.empty(final_length, dtype=int)
key_bob = np.empty(final_length, dtype=int)
for i in range(final_length):
	key_alice[i] = np.mod(np.sum(blocks_alice[i]), 2)
	key_bob  [i] = np.mod(np.sum(blocks_bob  [i]), 2)
print ('Final key: Alice has', key_alice, 'and Bob has', key_bob)

### The End
motors.write(b'go 0 0 0\n')
motors.flush()
time.sleep(1)
motors.close()
oscilloscope.close()
rm.close()
