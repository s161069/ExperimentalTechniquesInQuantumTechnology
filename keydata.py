import re
import sys
import numpy as np

### Settings
number_of_passes = 4 # Error correction

### Load data
assert len(sys.argv) == 2
prog = re.compile('.*Alice has ([01]) and Bob has ([01])')
alice = []
bob = []
for line in open(sys.argv[1]):
	result = prog.match(line)
	if result:
		alice.append(int(result.group(1)))
		bob.append(int(result.group(2)))
initial_bits_alice = np.array(alice)
initial_bits_bob   = np.array(bob)
initial_bits_needed = len(initial_bits_alice)

### Error rate
error_bits_alice = initial_bits_alice[1::2]
error_bits_bob = initial_bits_bob[1::2]
error_bits = np.abs(error_bits_alice - error_bits_bob) # Connection over classical channel
error_rate = np.sum(error_bits) / len(error_bits)
print ('Error rate:', error_rate)

### Initial key
initial_key_alice = initial_bits_alice[0::2]
initial_key_bob = initial_bits_bob[0::2]
initial_key_length = int(np.ceil(initial_bits_needed / 2))
print ('Initial key: Alice has\n', initial_key_alice, '\nand Bob has\n', initial_key_bob)

### Error correction
# Cascade protocol, see www.dtic.mil/dtic/tr/fulltext/u2/a557404.pdf for description of algorithm
public_bits = 0

# Divide key into blocks
blocks = [] # Array of indeces for initial_key arrays, 3D (pass, block, index)
for i in range(number_of_passes):
	number_of_blocks = int(np.ceil(np.sqrt(initial_key_length) / np.power(2, i)))
	permutation = np.random.permutation(initial_key_length) # Connection over classical channel
	block_edges = np.round(np.arange(1, number_of_blocks) * initial_key_length / number_of_blocks).astype(int)
	blocks.append(np.split(permutation, block_edges))

# Binary search to find the faulty bit
def correct_error(pass_number, block_number, start, end):
	global public_bits

	# Need to check if faulty bit is in given range
	parity_alice = np.mod(np.sum(initial_key_alice[blocks[pass_number][block_number][start:end]]), 2)
	parity_bob   = np.mod(np.sum(initial_key_bob  [blocks[pass_number][block_number][start:end]]), 2)
	error = np.abs(parity_alice - parity_bob) # Connection over classical channel
	public_bits = public_bits + 1

	if error:
		print ('Found faulty bit in pass', pass_number, ' block', block_number, 'between', start, 'and', end)
		# Locate faulty bit
		if start == end - 1:
			# Found the faulty bit
			initial_key_bob[blocks[pass_number][block_number][start]] = 1 - initial_key_bob[blocks[pass_number][block_number][start]]
			print ('Fixed faulty bit')
		else:
			# Continue binary search recursively
			mid = int(np.floor((end - start + 1) / 2 + start))
			first = correct_error(pass_number, block_number, start, mid)
			if not first:
				correct_error(pass_number, block_number, mid, end)
	return error

# Run error correction
found_errors = True
while found_errors:
	found_errors = False
	for pass_number in range(number_of_passes):
		for block_number in range(len(blocks[pass_number])):
			found_errors = correct_error(pass_number, block_number, 0, len(blocks[pass_number][block_number])) or found_errors
		if found_errors and pass_number > 1:
			break

# Result
print ('Error corrected key: Alice has\n', initial_key_alice, '\nand Bob has\n', initial_key_bob)
print ('Made', public_bits, 'bits public')

### Privacy amplification
final_length = initial_key_length - public_bits
block_edges = np.round(np.arange(1, final_length) * initial_key_length / final_length).astype(int)
blocks_alice = np.split(initial_key_alice, block_edges)
blocks_bob   = np.split(initial_key_bob,   block_edges)
key_alice = np.empty(final_length, dtype=int)
key_bob = np.empty(final_length, dtype=int)
for i in range(final_length):
	key_alice[i] = np.mod(np.sum(blocks_alice[i]), 2)
	key_bob  [i] = np.mod(np.sum(blocks_bob  [i]), 2)
print ('Final key: Alice has', key_alice, 'and Bob has', key_bob)
