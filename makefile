all: libg2.so

lib%.so: %.cpp
	g++ $< -o $@ -shared -fpic -Wall -Wextra -std=c++11 -O3
lib%.so: %.c
	gcc $< -o $@ -shared -fpic -Wall -Wextra -std=c11 -O3
