import numpy as np
import matplotlib.pyplot as plt
import visa

# Settings
time_range = 0.06 # microseconds
voltage_offset = -10 # volts
voltage_range = 20 # volts

# Open connection
rm = visa.ResourceManager()
inst = rm.open_resource('USB0::0x2A8D::0x1766::MY56311270::INSTR')

# Initialize
inst.write(':ACQuire:TYPE NORMal')
inst.write(':TIMebase:MODE MAIN')
inst.write(':TIMebase:RANGe ' + str(time_range / 1000000))
inst.write(':WAVeform:BYTeorder LSBFirst') # little-endian
inst.write(':WAVeform:FORMat BYTE')        # 8 bits
inst.write(':WAVeform:TYPE NORM')
inst.write(':WAVeform:UNSigned ON')
inst.write(':WAVeform:POINTS:MODE MAXimum')
inst.write(':WAVeform:POINTS 8000000')
inst.write(':CHANnel1:IMPedance FIFTy')
inst.write(':CHANnel3:IMPedance FIFTy')
inst.write(':CHANnel1:RANGe ' + str(voltage_range) + 'V')
inst.write(':CHANnel3:RANGe ' + str(voltage_range) + 'V')
#inst.write(':CHAnnel1:OFFSet ' + str(voltage_offset) + 'V')
#inst.write(':CHAnnel3:OFFSet ' + str(voltage_offset) + 'V')

# Run
inst.write(':SINGle CHANnel1,CHANnel3')
inst.write(':WAVeform:SOURce CHANnel1')
first = inst.query_binary_values(':WAVeform:DATA?', container=np.array, is_big_endian=False, datatype='B')
inst.write(':WAVeform:SOURce CHANnel3')
second = inst.query_binary_values(':WAVeform:DATA?', container=np.array, is_big_endian=False, datatype='B')
print('Number of samples per channel:', len(first))

# Plot
t = np.linspace(0, time_range, len(first))
plt.xlabel('time ($\mu s$)')
plt.ylabel('voltage (V)')
plt.plot(t, first * (voltage_range / 2**8) + voltage_offset, t, second * (voltage_range / 2**8) + voltage_offset)
plt.show()
