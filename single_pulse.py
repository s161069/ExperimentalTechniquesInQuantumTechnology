# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt

data = np.load('data/efficiency.npz')

i = 0
j = 100

y = data['channel1'][i][j]
z = data['channel2'][i][j]

y -= np.min(y)
z -= np.min(z)

x = np.linspace(0, 60, len(y))
z /= np.max(y)
y /= np.max(y)

#plt.xticks([0.4,0.14,0.2,0.2], fontsize = 50)
axis_font = {'fontname':'Arial', 'size':'16'}

plt.ylabel('voltage (relative units)',**axis_font)
plt.xlabel('t (ns)',**axis_font)
plt.plot(x, y, x, z)
plt.savefig('single.pdf')
plt.show()
