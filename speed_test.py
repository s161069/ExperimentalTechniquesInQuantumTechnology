import serial
import time

# Open
motors = serial.Serial(
	port='/dev/tty.usbserial-AIW50CK',
	baudrate=115200,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS
)
motors.close()
motors.open()
assert motors.is_open
motors.reset_input_buffer()
motors.reset_output_buffer()

# Move
motors.write(b'setMotor 0\n')
motors.write(b'go 0\n')
time.sleep(1)
motors.write(b'go 4800\n')
motors.flush()
motors.read(2)

# Measure
running = True
t1 = time.time()
while running:
	motors.write(b'm\n')
	r = motors.read(2)
	print (r)
	running = r == b'1\n'
print (time.time() - t1, 's')
