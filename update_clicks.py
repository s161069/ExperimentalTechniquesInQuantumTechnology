import os
from glob import glob
import numpy as np

for data_file in glob('segments/*.npz'):
	clicks_file = data_file.replace('segments', 'clicks')
	assert data_file != clicks_file
	assert os.path.isfile(data_file)
	assert os.path.isfile(clicks_file)

	data = np.load(data_file)
	clicks = np.load(clicks_file)

	if not 'number_of_bins' in clicks.files:
		channel1 = data['channel1']
		positions1 = clicks['channel1']
		positions2 = clicks['channel2']
		number_of_bins = []
		for run in range(len(channel1)):
			number_of_bins.append(len(channel1[run]))
		assert len(positions1) == len(number_of_bins)
		np.savez(clicks_file, channel1=positions1, channel2=positions2, number_of_bins=number_of_bins)
